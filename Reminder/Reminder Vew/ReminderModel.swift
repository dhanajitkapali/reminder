//
//  ReminderModel.swift
//  Reminder
//
//  Created by unthinkable-mac-0025 on 08/06/21.
//

import Foundation

struct ReminderModel {
    let title : String
    let body : String
    let date : Date
}
