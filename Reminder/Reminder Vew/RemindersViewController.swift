//
//  ViewController.swift
//  Reminder
//
//  Created by unthinkable-mac-0025 on 08/06/21.
//

import UIKit
import UserNotifications

class RemindersViewController: UIViewController {

    @IBOutlet var remindersTableView: UITableView!
    
    var remindersArray = [ReminderModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        remindersTableView.delegate = self
        remindersTableView.dataSource = self
    }

    @IBAction func testButtonPressed(_ sender: UIBarButtonItem) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (permissionGranted, error) in
            if permissionGranted{
                self.scheduleNotification()
            }else if let err = error{
                print("Error getting Ntification Permission \(err)")
            }
        }
    }
    @IBAction func addNewReminderButtonPressed(_ sender: UIBarButtonItem) {
        
    }
    
    func scheduleNotification()  {
        //1)request ,  2)set body , 3)add trigger/schedule
        let content = UNMutableNotificationContent()
        content.title = "Test Notification"
        content.sound = .default
        content.body = "This is the body of the test Notification"
        
        let targetDate = Date().addingTimeInterval(10)
//        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.year, . month], from: ), repeats: false)
    }
    
}

extension RemindersViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = remindersTableView.dequeueReusableCell(withIdentifier: "remindersCellID", for: indexPath)
        cell.textLabel?.text = "lol"
        
        return cell
    }
    
    
}

